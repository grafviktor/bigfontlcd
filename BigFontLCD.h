/* ************************************************************************************************* *
 * Based on Adrian Jones project "BIG FONT (2-line) LCD CHARACTERS"
 * http://woodsgood.ca/projects/2015/02/17/big-font-lcd-characters/
 * ************************************************************************************************* */

#ifndef BigFontLCD_h

  #define BigFontLCD_h
  
  #include <Arduino.h>
  #include <avr/pgmspace.h>            // for memory storage in program space
  #include <LiquidCrystal_I2C.h>
  
  class BigFontLCD {
    public:
      BigFontLCD(LiquidCrystal_I2C *lcd);
      void print(char *str, byte x, byte y);
  
    private:
      int writeBigChar(char ch, byte x, byte y);
  
      LiquidCrystal_I2C *lcd;
      byte col, row, nb = 0, bc = 0;                            // general
      byte bb[8];                                               // byte buffer for reading from PROGMEM
  
  };

#endif
